package main

import (
	"log"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	e := echo.New()

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD},
	}))

	e.GET("/fetch", func(c echo.Context) error {

		//**url to fetch is in command line argument
		// Build the request
		req, err := http.NewRequest("GET", os.Args[1], nil)
		check(err)

		// create a Client
		client := &http.Client{}

		// Do sends an HTTP request and
		resp, err := client.Do(req)
		check(err)

		// Defer the closing of the body
		defer resp.Body.Close()

		return c.Stream(http.StatusOK, "application/json", resp.Body)
	})

	e.Logger.Fatal(e.Start(os.Args[2]))

}
